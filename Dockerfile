FROM wyveo/nginx-php-fpm:php80
WORKDIR /usr/share/nginx/html
COPY . /usr/share/nginx/html
EXPOSE 80
RUN chmod 666 .data/config.php \
	&& sed -i '32 i\        rewrite ^/(?!.well-known)(.*)$ /index.php?/$1 last;' /etc/nginx/conf.d/default.conf \
#docker build -t onedrive . --no-cache
#docker run --restart=always -p 80:80 -d onedrive